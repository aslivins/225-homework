tpr <- tpr[sorted_indices]
# Compute AUC using the trapezoidal rule
auc <- trapz(fpr, tpr)
return(auc)
}
AUC <- array(0, dim = c(4, 6))
for(class in 1:3){
ROC <- data.frame(cbind(CVFPR[,,class], CVTPR[,,class]))
AUC[class, 1] <- compute_auc(ROC$X1, ROC$X6)
AUC[class, 2] <- compute_auc(ROC$X2, ROC$X7)
AUC[class, 3] <- compute_auc(ROC$X3, ROC$X8)
AUC[class, 4] <- compute_auc(ROC$X4, ROC$X9)
AUC[class, 5] <- compute_auc(ROC$X5, ROC$X10)
AUC[class, 6] <- mean(AUC[class, 1:5])
}
for(mm in 1:6){
AUC[4,mm] <- mean(AUC[1:3,mm])
}
round(AUC, 4)
table_res <- array(0, dim = c(5, 8))
for(b in 1:3){
table_res[1:5, b] <- round(CVmisc_rates2, 4)[,2,b]
table_res[1:5, (b + 4)] <- round(AUC, 4)[b, 1:5]
}
table_res[1:5, 4] <- round(CVmisc_rates[1:5,2], 4)
table_res[1:5, 8] <- round(AUC, 4)[4, 1:5]
feat_final <- array(0, dim = c((length(vars)-1)))
for(b in 1:(length(vars)-1)){
feat_final[b] <- mean(feat_imp[,,b])
}
pltdf <- data.frame(x = c(1:(length(vars)-1)), y = feat_final)
bb <- colnames(data[,vars[-length(vars)]])
bb[1] <- 'Marital.status'
#bb <- strtrim(bb, 8)
ggplot(pltdf, aes(x = y, y = x)) + geom_point() +
scale_y_continuous(breaks = c(1:32), labels = bb) +
theme(axis.text.y = element_text(size = 12)) +
ggtitle('Feature Importance: Entire Dataset')
#bb <- strtrim(bb, 8)
ggplot(pltdf, aes(x = y, y = x)) + geom_point() +
scale_y_continuous(breaks = c(1:32), labels = bb) +
theme(axis.text.y = element_text(size = 12)) +
ggtitle('Feature Importance: Entire Dataset')+ xlab('Mean decrease in accuracy of Dropout class')
# Author: Adam Slivinsky
# Work: Last Code
# Date: 28 March 2024
# Clear Data Environment
rm(list=ls(all=TRUE))
set.seed(7)
# Load Libraries
library(ggplot2)
library(Matrix)
library(mvtnorm)
library(reshape2)
library(nnet)
library(pracma)
library(ipred)
library(randomForest)
library(smotefamily)
#Load Data
data <- read.csv('academic_data.csv')
#Dropouts
data_d <- data[which(data$Target == 'Dropout'),]
#Enrolled
data_e <- data[which(data$Target == 'Enrolled'),]
#Graduates
data_g <- data[which(data$Target == 'Graduate'),]
#Plot bar chart of how many are enrolled/graduated/dropped out
ggplot(data.frame(status=c('Dropout', 'Enrolled', 'Graduate'),
num=c(length(which(data$Target == 'Dropout')),
length(which(data$Target == 'Enrolled')),
length(which(data$Target == 'Graduate')))),
aes(x=status, y=num)) + geom_bar(stat='identity') +
xlab('Student Status') + ylab('Number of Students') +
ggtitle('Number of class labels in \nAcademic Performance dataset')
#5-fold Cross-validation
vars <- c(1, 6:37)
data <- data[sample(1:nrow(data)), ]
n_folds <- 5
repeats <- 3
fold_ind = (((1:nrow(data)) %% n_folds) + 1)
K <- length(unique(data$Target))
p <- length(vars) - 1
groups = c('Dropout', 'Enrolled', 'Graduate')
n_models <- 5
max_len <- 10000
#Tables for size of duplication
before_tab <- array(0, dim = c(n_folds, repeats, 3))
after_tab <- array(0, dim = c(n_folds, repeats, 2))
# LDA misclassification
Training_discriminants <- array(0, dim = c(K, max_len, n_models, n_folds, repeats))
Testing_discriminants <- array(0, dim = c(K, max_len, n_models, n_folds, repeats))
Y_hat_TR <- array(0, dim = c(max_len, n_models, n_folds, repeats))
Y_hat_TE <- array(0, dim = c(max_len, n_models, n_folds, repeats))
misclassification_rates <- array(0, dim = c(n_models,2, n_folds, repeats))
misclassification_rates2 <- array(0, dim = c(n_models,2,K, n_folds, repeats))
CVmisc_rates <- array(0, dim = c(n_models, 2))
CVmisc_rates2 <- array(0, dim = c(n_models, 2, K))
feat_imp <- array(0, dim = c(n_folds, repeats, (length(vars)-1)))
# ROC Curves
prob6 <- array(0, dim = c(max_len, n_models, K, n_folds, repeats))
tau <- seq(0, 1, by=1e-3)
TP <- array(0, dim = c(length(tau), n_models, K, n_folds, repeats))
FP <- array(0, dim = c(length(tau), n_models, K, n_folds, repeats))
TN <- array(0, dim = c(length(tau), n_models, K, n_folds, repeats))
FN <- array(0, dim = c(length(tau), n_models, K, n_folds, repeats))
FPR <- array(0, dim = c(length(tau), n_models, K, n_folds, repeats))
TPR <- array(0, dim = c(length(tau), n_models, K, n_folds, repeats))
CVFPR <- array(0, dim = c(length(tau), n_models, K))
CVTPR <- array(0, dim = c(length(tau), n_models, K))
for(r in 1:repeats){
data <- data[sample(1:nrow(data)), ]
DTrainCV <- array(0, dim=c(length(which(fold_ind != 1)),
length(vars), n_folds))
DTestCV <- array(0, dim=c(length(which(fold_ind == 2)),
length(vars), n_folds))
DTrainLen <- array(0, dim = c(n_folds))
DTestLen <- array(0, dim = c(n_folds))
for(nf in 1:n_folds){
DTrainLen[nf] <- length(which(fold_ind != nf))[1]
DTestLen[nf] <- length(which(fold_ind == nf))[1]
DTrainCV[1:DTrainLen[nf],,nf] <- data.matrix((data[which(fold_ind != nf),vars]))
DTestCV[1:DTestLen[nf],,nf] <- data.matrix(data[which(fold_ind == nf),vars])
}
for(nf in 1:n_folds){
#Get testing and training splits
Training = DTrainCV[1:DTrainLen[nf],,nf]
before_tab[nf, r, ] <- as.numeric(table(Training[,33]))
minor <- array(0, dim = c(DTrainLen[nf]))
for(mm in which(Training[,33] == 1)){
minor[mm] <- 1
}
# Using SMOTE to balance the dataset
balanced_data <- ADAS(data.frame(Training[,-33]), as.factor(minor), K = 5)
after_tab[nf, r, ] <- as.numeric(table(balanced_data$data[,33]))
balanced_data$data[,33] <- as.numeric(balanced_data$data[,33])
balanced_data$data[(sum((balanced_data$data[,33] )) + 1):
sum(after_tab[nf, r, ]),33] <- Training[which(Training[,33] != 1), 33]
Training <- balanced_data$data
Training <- (Training[sample(1:nrow(Training)),])
Training <- as.matrix(Training)
Testing = DTestCV[1:DTestLen[nf],,nf]
group_means <- array(0, dim = c(K,p))
group_cov <- array(0, dim = c(K,p,p))
group_cov_inv <- array(0, dim = c(K,p,p))
common_cov <- array(0, dim = c(p, p))
group_pi <- array(0, dim = c(K))
for(i in 1:K){
group_train <- Training[which(Training[,length(vars)] == i),]
for(j in 1:p){
group_means[i,j] <- mean(group_train[,j])
}
for(l in 1:length(group_train[,1])){
contribution <- as.numeric(group_train[l,1:p] - group_means[i,]) %*%
t(as.numeric(group_train[l,1:p] - group_means[i,]))
group_cov[i,,] <- group_cov[i,,] + contribution
common_cov <- common_cov + contribution
}
group_cov[i,,] <- group_cov[i,,] / length(group_train[,1])
group_cov_inv[i,,] <- chol2inv(chol(group_cov[i,,]))
group_pi[i] <- length(group_train[,1]) / length(Training[,1])
}
common_cov <- common_cov / length(Training[,1])
common_cov_inv <- chol2inv(chol(common_cov))
rm(group_train)
# Loops to calculate discriminants (1 for training, 1 for testing)
for(k in 1:K){
for(n1 in 1:length(Training[,1])){
X = as.numeric(Training[n1, 1:p])
# LDA discriminant
Training_discriminants[k, n1, 1, nf, r] <- t(X) %*% common_cov_inv %*% group_means[k,] +
(-1/2) * t(group_means[k,]) %*% common_cov_inv %*% group_means[k,] +
log(group_pi[k])
# QDA discriminant
Training_discriminants[k, n1, 2, nf, r] <- (-1/2) * log(det(group_cov[k,,])) +
(-1/2) * t(X - group_means[k,]) %*% group_cov_inv[k,,] %*%
(X - group_means[k,]) + log(group_pi[k])
}
for(n2 in 1:length(Testing[,1])){
X = as.numeric(Testing[n2, 1:p])
# LDA discriminant
Testing_discriminants[k, n2, 1, nf, r] <- t(X) %*% common_cov_inv %*% group_means[k,] +
(-1/2) * t(group_means[k,]) %*% common_cov_inv %*% group_means[k,] +
log(group_pi[k])
# QDA discriminant
Testing_discriminants[k, n2, 2, nf, r] <- (-1/2) * log(det(group_cov[k,,])) +
(-1/2) * t(X - group_means[k,]) %*% group_cov_inv[k,,] %*%
(X - group_means[k,]) + log(group_pi[k])
}
}
# Logistic Regression (fit multinomial model, get predictions)
Y_tr <- as.numeric(factor(Training[,length(vars)]))
Y_te <- as.numeric(factor(Testing[,length(vars)]))
LR <- multinom(class~ ., data=data.frame(Training))
Y_hat_TR[(1:length(Training[,1])),3, nf, r] <- as.numeric(predict(LR, data.frame(Training)))
Y_hat_TE[(1:length(Testing[,1])),3, nf, r] <- as.numeric(predict(LR, data.frame(Testing)))
#Loops to classify according to discriminants
# Classify according to highest discriminant
for(n1 in 1:length(Training[,1])){
for(model in 1:2){
Y_hat_TR[n1, model, nf, r] <- which(Training_discriminants[,n1,model, nf, r] ==
max(Training_discriminants[,n1,model, nf, r]))
if(Y_tr[n1] == Y_hat_TR[n1,model,nf, r]){
misclassification_rates[model, 1,nf, r] <- misclassification_rates[model, 1,nf, r] + 1
}
}
if(Y_tr[n1] == Y_hat_TR[n1, 3,nf, r]){
misclassification_rates[3,1,nf, r] <- misclassification_rates[3,1,nf, r] + 1
}
}
for(n2 in 1:length(Testing[,1])){
for(model in 1:2){
Y_hat_TE[n2, model, nf, r] <- which(Testing_discriminants[,n2,model, nf, r] ==
max(Testing_discriminants[,n2,model, nf, r]))
if(Y_te[n2] == Y_hat_TE[n2,model, nf, r]){
misclassification_rates[model, 2, nf, r] <- misclassification_rates[model, 2, nf, r] + 1
}
}
if(Y_te[n2] == Y_hat_TE[n2, 3, nf, r]){
misclassification_rates[3,2, nf, r] <- misclassification_rates[3,2, nf, r] + 1
}
}
misclassification_rates[1:3,1, nf, r] <- misclassification_rates[1:3,1, nf, r] / length(Training[,1])
misclassification_rates[1:3,2, nf, r] <- misclassification_rates[1:3,2, nf, r] / length(Testing[,1])
misclassification_rates[1:3,, nf, r] <- 1 - misclassification_rates[1:3,, nf, r]
#misclassification_rates
for(class in 1:K){
#Loops to classify according to discriminants
# Classify according to highest discriminant
for(n1 in 1:length(Training[,1])){
if(Y_tr[n1] != class){
next
}
for(model in 1:2){
Y_hat_TR[n1, model, nf, r] <- which(Training_discriminants[,n1,model, nf, r] ==
max(Training_discriminants[,n1,model, nf, r]))
if(Y_tr[n1] == Y_hat_TR[n1,model, nf, r]){
misclassification_rates2[model, 1, class, nf, r] <- misclassification_rates2[model, 1, class, nf, r] + 1
}
}
if(Y_tr[n1] == Y_hat_TR[n1, 3, nf, r]){
misclassification_rates2[3,1, class, nf, r] <- misclassification_rates2[3,1, class, nf, r] + 1
}
}
for(n2 in 1:length(Testing[,1])){
if(Y_te[n2] != class){
next
}
for(model in 1:2){
Y_hat_TE[n2, model, nf,r] <- which(Testing_discriminants[,n2,model, nf, r] ==
max(Testing_discriminants[,n2,model, nf, r]))
if(Y_te[n2] == Y_hat_TE[n2,model, nf, r]){
misclassification_rates2[model, 2, class, nf, r] <- misclassification_rates2[model, 2, class, nf, r] + 1
}
}
if(Y_te[n2] == Y_hat_TE[n2, 3, nf, r]){
misclassification_rates2[3,2, class, nf, r] <- misclassification_rates2[3,2, class, nf, r] + 1
}
}
misclassification_rates2[1:3,1, class, nf, r] <- misclassification_rates2[1:3,1, class, nf, r] /
length(which(Y_tr == class))
misclassification_rates2[1:3,2, class, nf, r] <- misclassification_rates2[1:3,2, class, nf, r] /
length(which(Y_te == class))
misclassification_rates2[1:3,,class, nf, r] <- 1 - misclassification_rates2[1:3,,class, nf, r]
}
#misclassification_rates2
#Get the probability of class 1 (prob6)
for(class in 1:3){
for(n2 in 1:length(Testing[,1])){
#LDA
prob6[n2,1, class, nf, r] <- dmvnorm(Testing[n2,1:p], group_means[class,], common_cov)*group_pi[class] /
(dmvnorm(Testing[n2,1:p], group_means[1,], common_cov)*group_pi[1] +
dmvnorm(Testing[n2,1:p], group_means[2,], common_cov)*group_pi[2] +
dmvnorm(Testing[n2,1:p], group_means[3,], common_cov)*group_pi[3])
#QDA
prob6[n2,2, class, nf, r] <- dmvnorm(Testing[n2,1:p], group_means[class,], group_cov[class,,])*group_pi[class] /
(dmvnorm(Testing[n2,1:p], group_means[1,], group_cov[1,,])*group_pi[1] +
dmvnorm(Testing[n2,1:p], group_means[2,], group_cov[2,,])*group_pi[2] +
dmvnorm(Testing[n2,1:p], group_means[3,], group_cov[3,,])*group_pi[3])
}
}
# Logistic Regression (fit multinomial model, get predictions)
df_k <- data.frame(Training)
LR <- multinom(class ~ ., data=df_k)
prob6[(1:length(Testing[,1])),3, 1, nf, r] <- as.numeric(predict(LR, data.frame(Testing), type='probs'))[1:length(Testing[,1])]
prob6[(1:length(Testing[,1])),3, 2, nf, r] <- as.numeric(predict(LR, data.frame(Testing), type='probs'))[(length(Testing[,1]) + 1) : (2*length(Testing[,1]))]
prob6[(1:length(Testing[,1])),3, 3, nf, r] <- as.numeric(predict(LR, data.frame(Testing), type='probs'))[((2* length(Testing[,1])) + 1): (3*length(Testing[,1]))]
#Get FPR and TPR
for(class in 1:3){
for(m in 1:3){
for(t in 1:length(tau)){
for(n2 in 1:length(Testing[,1])){
if(prob6[n2, m, class, nf, r] > tau[t]){
y_hat <- class
if(Y_te[n2] == y_hat){
TP[t, m, class, nf, r] <- TP[t,m, class, nf, r] + 1
}
if(Y_te[n2] != y_hat){
FP[t,m, class, nf, r] <- FP[t,m, class, nf, r] + 1
}
}
if(prob6[n2, m, class, nf, r] <= tau[t]){
y_hat <- class
if(Y_te[n2] != y_hat){
TN[t, m, class, nf, r] <- TN[t,m, class, nf, r] + 1
}
if(Y_te[n2] == y_hat){
FN[t,m, class, nf, r] <- FN[t,m, class, nf, r] + 1
}
}
}
FPR[t,m, class, nf, r] <- FP[t,m, class, nf, r] / (FP[t,m, class, nf, r] + TN[t,m, class, nf, r])
TPR[t,m, class, nf, r] <- TP[t,m, class, nf, r] / (TP[t,m, class, nf, r] + FN[t,m, class, nf, r])
}
}
}
}
tr_df <- data.frame(Training)
te_df <- data.frame(Testing)
tr_df$X33 <- as.factor(tr_df$class)
te_df$X33 <- as.factor(te_df$X33)
bagg <- ipredbagg(y=tr_df$X33,
X=tr_df[,(1:p)], nbagg = 25, coob = TRUE)
forr <- randomForest(y=tr_df$X33,
x=tr_df[,(1:p)], ntrees=500, importance = TRUE)
Y_hat_TR[1:length(Training[,1]),4,nf,r] <- predict(bagg, tr_df[,(1:p)],
type='class', aggregation = "majority")
Y_hat_TE[1:length(Testing[,1]), 4, nf, r] <- predict(bagg, te_df[,(1:p)],
type='class', aggregation = "majority")
Y_hat_TR[1:length(Training[,1]),5,nf,r] <- as.numeric(predict(forr, tr_df[,(1:p)],
type="response"))
Y_hat_TE[1:length(Testing[,1]), 5, nf, r] <- as.numeric(predict(forr, te_df[,(1:p)],
type="response"))
prob6[(1:length(Testing[,1])),4, 1, nf, r] <- as.numeric(predict(bagg, te_df[,(1:p)], type='prob'))[(1:length(Testing[,1]))]
prob6[(1:length(Testing[,1])),4, 2, nf, r] <- as.numeric(predict(bagg, te_df[,(1:p)], type='prob'))[(length(Testing[,1]) + 1) : (2*length(Testing[,1]))]
prob6[(1:length(Testing[,1])),4, 3, nf, r] <- as.numeric(predict(bagg, te_df[,(1:p)], type='prob'))[((2* length(Testing[,1])) + 1): (3*length(Testing[,1]))]
prob6[(1:length(Testing[,1])),5, 1, nf, r] <- as.numeric(predict(forr, te_df[,(1:p)], type='prob')[,1])
prob6[(1:length(Testing[,1])),5, 2, nf, r] <- as.numeric(predict(forr, te_df[,(1:p)], type='prob')[,2])
prob6[(1:length(Testing[,1])),5, 3, nf, r] <- as.numeric(predict(forr, te_df[,(1:p)], type='prob')[,3])
for(n1 in 1:length(Training[,1])){
if(Y_tr[n1] == Y_hat_TR[n1,4,nf, r]){
misclassification_rates[4, 1,nf, r] <- misclassification_rates[4, 1,nf, r] + 1
}
if(Y_tr[n1] == Y_hat_TR[n1,5,nf, r]){
misclassification_rates[5, 1,nf, r] <- misclassification_rates[5, 1,nf, r] + 1
}
}
for(n2 in 1:length(Testing[,1])){
if(Y_te[n2] == Y_hat_TE[n2,4, nf, r]){
misclassification_rates[4, 2, nf, r] <- misclassification_rates[4, 2, nf, r] + 1
}
if(Y_te[n2] == Y_hat_TE[n2,5, nf, r]){
misclassification_rates[5, 2, nf, r] <- misclassification_rates[5, 2, nf, r] + 1
}
}
for(class in 1:K){
#Loops to classify according to discriminants
# Classify according to highest discriminant
for(n1 in 1:length(Training[,1])){
if(Y_tr[n1] != class){
next
}
if(Y_tr[n1] == Y_hat_TR[n1,4, nf, r]){
misclassification_rates2[4, 1, class, nf, r] <- misclassification_rates2[4, 1, class, nf, r] + 1
}
if(Y_tr[n1] == Y_hat_TR[n1,5, nf, r]){
misclassification_rates2[5, 1, class, nf, r] <- misclassification_rates2[5, 1, class, nf, r] + 1
}
}
for(n2 in 1:length(Testing[,1])){
if(Y_te[n2] != class){
next
}
if(Y_te[n2] == Y_hat_TE[n2,4, nf, r]){
misclassification_rates2[4, 2, class, nf, r] <- misclassification_rates2[4, 2, class, nf, r] + 1
}
if(Y_te[n2] == Y_hat_TE[n2,5, nf, r]){
misclassification_rates2[5, 2, class, nf, r] <- misclassification_rates2[5, 2, class, nf, r] + 1
}
}
misclassification_rates2[4:5,1, class, nf, r] <- misclassification_rates2[4:5,1, class, nf, r] /
length(which(Y_tr == class))
misclassification_rates2[4:5,2, class, nf, r] <- misclassification_rates2[4:5,2, class, nf, r] /
length(which(Y_te == class))
misclassification_rates2[4:5,,class, nf, r] <- 1 - misclassification_rates2[4:5,,class, nf, r]
}
#Get FPR and TPR
for(class in 1:3){
for(m in 4:5){
for(t in 1:length(tau)){
for(n2 in 1:length(Testing[,1])){
if(prob6[n2, m, class, nf, r] > tau[t]){
y_hat <- class
if(Y_te[n2] == y_hat){
TP[t, m, class, nf, r] <- TP[t,m, class, nf, r] + 1
}
if(Y_te[n2] != y_hat){
FP[t,m, class, nf, r] <- FP[t,m, class, nf, r] + 1
}
}
if(prob6[n2, m, class, nf, r] <= tau[t]){
y_hat <- class
if(Y_te[n2] != y_hat){
TN[t, m, class, nf, r] <- TN[t,m, class, nf, r] + 1
}
if(Y_te[n2] == y_hat){
FN[t,m, class, nf, r] <- FN[t,m, class, nf, r] + 1
}
}
}
FPR[t,m, class, nf, r] <- FP[t,m, class, nf, r] / (FP[t,m, class, nf, r] + TN[t,m, class, nf, r])
TPR[t,m, class, nf, r] <- TP[t,m, class, nf, r] / (TP[t,m, class, nf, r] + FN[t,m, class, nf, r])
}
}
}
misclassification_rates[(4:5),1, nf, r] <- misclassification_rates[(4:5),1, nf, r] / length(Training[,1])
misclassification_rates[(4:5),2, nf, r] <- misclassification_rates[(4:5),2, nf, r] / length(Testing[,1])
misclassification_rates[(4:5),, nf, r] <- 1 - misclassification_rates[(4:5),, nf, r]
#misclassification_rates
feat_imp[nf, r,] <- forr$importance[,1]
}
for(class in 1:3){
for(m in 1:3){
for(t in 1:length(tau)){
CVFPR[t,m,class] <- mean(FPR[t,m,class,,])
CVTPR[t,m,class] <- mean(TPR[t,m,class,,])
}
}
for(m in 4:5){
for(t in 1:length(tau)){
CVFPR[t,m,class] <- mean(FPR[t,m,class,nf,])
CVTPR[t,m,class] <- mean(TPR[t,m,class,nf,])
}
}
}
for(m in 1:3){
for(q in 1:2){
CVmisc_rates[m, q] <- mean(misclassification_rates[m, q, ,])
for(w in 1:3){
CVmisc_rates2[m, q, w] <- mean(misclassification_rates2[m, q, w,, ])
}
}
}
for(m in 4:5){
for(q in 1:2){
CVmisc_rates[m,q] <- mean(misclassification_rates[m,q,nf,])
for(w in 1:3){
CVmisc_rates2[m, q, w] <- mean(misclassification_rates2[m, q, w, nf, ])
}
}
}
round(CVmisc_rates, 4)
round(CVmisc_rates2, 4)
#PLOT ROC
class = 1
groups2 <- c('Dropouts', 'Enrolled', 'Graduates')
ROC <- data.frame(cbind(CVFPR[,,class], CVTPR[,,class]))
ggplot(ROC, aes(x=X1, y = X6)) + geom_path(color='black') +
geom_path(aes(x=X2, y = X7), color='blue') +
geom_path(aes(x=X3, y = X8), color='red') +
geom_path(aes(x=X4, y = X9), color='green') +
geom_path(aes(x=X5, y = X10), color='brown') + xlab('FPR') +
ylab('TPR') + ggtitle(paste0('ROC Curves for ', groups2[class] )) +
geom_abline(intercept = 1, slope = -1, color = "black", linetype = "dashed", lwd = 1)
#Compute AUC
compute_auc <- function(fpr, tpr) {
# Ensure FPR and TPR are sorted by FPR
sorted_indices <- order(fpr)
fpr <- fpr[sorted_indices]
tpr <- tpr[sorted_indices]
# Compute AUC using the trapezoidal rule
auc <- trapz(fpr, tpr)
return(auc)
}
AUC <- array(0, dim = c(4, 6))
for(class in 1:3){
ROC <- data.frame(cbind(CVFPR[,,class], CVTPR[,,class]))
AUC[class, 1] <- compute_auc(ROC$X1, ROC$X6)
AUC[class, 2] <- compute_auc(ROC$X2, ROC$X7)
AUC[class, 3] <- compute_auc(ROC$X3, ROC$X8)
AUC[class, 4] <- compute_auc(ROC$X4, ROC$X9)
AUC[class, 5] <- compute_auc(ROC$X5, ROC$X10)
AUC[class, 6] <- mean(AUC[class, 1:5])
}
for(mm in 1:6){
AUC[4,mm] <- mean(AUC[1:3,mm])
}
round(AUC, 4)
table_res <- array(0, dim = c(5, 8))
for(b in 1:3){
table_res[1:5, b] <- round(CVmisc_rates2, 4)[,2,b]
table_res[1:5, (b + 4)] <- round(AUC, 4)[b, 1:5]
}
table_res[1:5, 4] <- round(CVmisc_rates[1:5,2], 4)
table_res[1:5, 8] <- round(AUC, 4)[4, 1:5]
feat_final <- array(0, dim = c((length(vars)-1)))
for(b in 1:(length(vars)-1)){
feat_final[b] <- mean(feat_imp[,,b])
}
pltdf <- data.frame(x = c(1:(length(vars)-1)), y = feat_final)
bb <- colnames(data[,vars[-length(vars)]])
bb[1] <- 'Marital.status'
#bb <- strtrim(bb, 8)
ggplot(pltdf, aes(x = y, y = x)) + geom_point() +
scale_y_continuous(breaks = c(1:32), labels = bb) +
theme(axis.text.y = element_text(size = 12)) +
ggtitle('Feature Importance: Entire Dataset')+ xlab('Mean decrease in accuracy of Dropout class')
#bb <- strtrim(bb, 8)
ggplot(pltdf, aes(x = y, y = x)) + geom_point() +
scale_y_continuous(breaks = c(1:32), labels = bb) +
theme(axis.text.y = element_text(size = 12)) +
ggtitle('Feature Importance: Entire Dataset')+ xlab('Mean decrease in accuracy of Dropout class')
