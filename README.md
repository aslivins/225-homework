# Effective Strategies for Balancing Class Distribution in Classification Problems 

This work is the Final Project for Multivariate Statistical Methods (STAT 225) at UCSC, where I study how to improve predictive performance of classifiers through over-sampling methods.  

## Introduction

When working with many real-world classification problems, one can often encounter data with class imbalances, meaning there are large differences between the number of observations of each class. Classification algorithms generally fit best to this type of data by prioritizing the fit of majority, or larger, classes over the minority, or smaller classes. However, it is often of interest to prioritize the fit of the minority classes, such as in fraud detection, in the modeling of rare diseases, and spam detection. Towards this interest, in this paper we discuss alternative sampling methods aimed to prioritize the fit of classification algorithms on minority classes. We introduce the ideas of over- and under-sampling, and consider two alternative oversampling methods over the naive method, SMOTE and ADASYN. These methods generate extra data for the minority classes using nearest neighbors. We also discuss the implementation and fit of different classification algorithms, including Linear Discriminant Analysis (LDA), Quadratic Discriminant Analysis (QDA), Logistic Regression, Bagging Decision Trees, and Random Forest...

**Keywords**: Classification, Class Imbalance, Oversampling, SMOTE, ADASYN
